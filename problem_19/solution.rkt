;;;
;;; Matching Brackets
;;;
;;; tetsumi@vmail.me
;;;

#lang racket

(define (check a b)
  (let ([ai (char->integer a)]
        [bi (char->integer b)])
    (not (or (= (- ai 1) bi) (= (- ai 2) bi)))))

(for ([i (string->number (read-line))])
  (printf "~A " (let loop ([stack '()]
                           [chars (string->list (read-line))])
                  (cond [(empty? chars) (or (and (empty? stack) 1) 0)]
                        [(member (car chars) '(#\) #\} #\] #\>))
                         (if (or (empty? stack)
                                 (check (car chars) (car stack)))
                             0
                             (loop (cdr stack) (cdr chars)))]
                        [(member (car chars) '(#\( #\{ #\[ #\<))
                         (loop (cons (car chars) stack) (cdr chars))]
                        [else (loop stack (cdr chars))]))))
         
